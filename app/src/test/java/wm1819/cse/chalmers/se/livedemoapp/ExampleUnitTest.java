package wm1819.cse.chalmers.se.livedemoapp;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void isGreaterThanTest() {
        MainActivity myTestClass = new MainActivity();
        assertEquals(true, myTestClass.isGreaterThan(5, 4));
    }

    @Test
    public void isGreaterThanTest2() {
        MainActivity myTestClass = new MainActivity();
        assertEquals(false, myTestClass.isGreaterThan(5, 5));
    }
}