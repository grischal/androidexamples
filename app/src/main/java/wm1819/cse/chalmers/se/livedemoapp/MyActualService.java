package wm1819.cse.chalmers.se.livedemoapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

public class MyActualService extends Service {
    private int started = 0;

    public MyActualService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (started<2) {
            Toast.makeText(this, "We're doing some work in this thread", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Already too much work...", Toast.LENGTH_SHORT).show();
        }

        started++;

        Intent sendMsgIntent = new Intent("SendFromService");
        sendMsgIntent.putExtra("started", started);
        LocalBroadcastManager.getInstance(this).sendBroadcast(sendMsgIntent);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service killed", Toast.LENGTH_SHORT).show();
    }
}
