package wm1819.cse.chalmers.se.livedemoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class NewMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.HTTP_PARAM);

        TextView text = findViewById(R.id.displayTextView);
        text.setText(message);
    }

    public void onClickThreadSafe (View view) {
        //Starts a new thread
        new Thread(new Runnable() {
            public void run () {
                //Get my text field
                final TextView text = findViewById(R.id.displayTextView);

                //Edit the text field.
                //We call the View.post method (which takes a new thread as parameter),
                // to explicitly run the code in the UI thread
                text.post(new Runnable () {
                    public void run () {
                        text.setText("New Text");
                    }
                });
            }
        }).start();
    }

    public void onClickNotThreadSafe (View view) {
        //Starts a new thread
        new Thread(new Runnable() {
            public void run () {
                //Get my text field
                TextView text = findViewById(R.id.displayTextView);

                //Edit the text field. As we do this in our new Thread (and not the UI thread),
                //this will cause an exception
                text.setText("New Text");
            }
        }).start();
    }
}
